#!/bin/bash
set -euo pipefail

export INVENTORY_HOSTS=$(hcloud server list -l Name=stream -o noheader -o columns=ipv4 | sed 's/\(.*\)/"\1"/g' | tr '\n' ',' | sed 's/,$//g' )
envsubst < inventory.json.tpl
