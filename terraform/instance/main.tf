resource "hcloud_server" "stream" {
  name = "stream"
  image = "debian-10"
  server_type = "cx21"
  ssh_keys = ["id_hetzner"]
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = "hyperion"})

  labels = {
    Name = "stream"
  }
}